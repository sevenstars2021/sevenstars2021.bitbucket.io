$(document).ready(function() {
    $("#destinations").click(function() {
        $(".homepage").load("destinations.html");
    });
});

$(document).ready(function() {
    $("#events").click(function() {
        $(".homepage").load("events.html #box");
    });
});

$(document).ready(function() {
    $("#activities").click(function() {
        $(".homepage").load("activities.html #box");
    });
});

$(document).ready(function() {
    $("#login").click(function() {
        $(".homepage").load("login.html #box");
    });
});

$(document).ready(function() {
    $("#register").click(function() {
        $(".homepage").load("register.html #box");
    });
});