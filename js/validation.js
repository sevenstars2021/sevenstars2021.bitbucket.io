function formValidation() {
    var name = document.registration.name;
    var email = document.registration.email;
    var password = document.registration.password;


    if (password_validation(password, 7, 12)) {
        if (allLetter(name)) {
            if (ValidateEmail(email)) {
                $('.homepage').load('./registerSuccess.html');
            }
        }
    }
    return false;
}

function formLoginValidation() {
    var loginEmail = document.login.lemail;
    var loginPassword = document.login.lpassword;


    if (password_validation(loginPassword, 7, 12)) {
        if (ValidateEmail(loginEmail)) {
            $(document).ready(function () {
                $('.homepage').load('./loginSuccess.html');
            })
        }
    }
    return false;
}


function password_validation(password, mx, my) {
    var password_len = password.value.length;
    if (password_len == 0 || password_len >= my || password_len < mx) {
        alert("Password should not be empty, or length be between " + mx + " to " + my);
        password.focus();
        return false;
    }
    return true;
}

function allLetter(name) {
    var letters = /^[A-Za-z ]+$/;
    if (name.value.match(letters)) {
        return true;
    } else {
        alert('Name must have alphabet characters only');
        name.focus();
        return false;
    }
}

function ValidateEmail(email) {
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (email.value.match(mailformat)) {
        return true;
    } else {
        alert("You have entered an invalid email address.");
        email.focus();
        return false;
    }
}

// $(document).ready(function(){
//     $('.btnSubmit').click(function(){
//         $('.homepage').load('./read.html');
//     });
// })