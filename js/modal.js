function actiOne() {
    // Load file from server
    $.ajax({
        url: "./actiOne.txt",
        success: function (result) {
            // Append file's content to modal-body & show myModal
            $(".modal-body").html(result);
            $("#myModal").modal('show');

        }
    });
}

function actiTwo() {
    // Load file from server
    $.ajax({
        url: "./actiTwo.txt",
        success: function (result) {
            // Append file's content to modal-body & show myModal
            $(".modal-body").html(result);
            $("#myModal").modal('show');

        }
    });
}

function actiThree() {
    // Load file from server
    $.ajax({
        url: "./actiThree.txt",
        success: function (result) {
            // Append file's content to modal-body & show myModal
            $(".modal-body").html(result);
            $("#myModal").modal('show');

        }
    });
}

function eventOne() {
    // Load file from server
    $.ajax({
        url: "./eventOne.txt",
        success: function (result) {
            // Append file's content to modal-body & show myModal
            $(".modal-body").html(result);
            $("#myModal").modal('show');

        }
    });
}

function eventTwo() {
    // Load file from server
    $.ajax({
        url: "./eventTwo.txt",
        success: function (result) {
            // Append file's content to modal-body & show myModal
            $(".modal-body").html(result);
            $("#myModal").modal('show');

        }
    });
}

function eventThree() {
    // Load file from server
    $.ajax({
        url: "./eventThree.txt",
        success: function (result) {
            // Append file's content to modal-body & show myModal
            $(".modal-body").html(result);
            $("#myModal").modal('show');

        }
    });
}