/* Destination*/

"use strict"

var i=0;
var text = 'Life is mostly lived outside in this sunny city. You can dine alfresco on fresh-caught seafood, stroll through lush botanic gardens.';
var speed = 30;


function typeWriter() {

if(i<text.length){
    document.getElementById("text").innerHTML += text.charAt(i);
    i++;
    setTimeout(typeWriter,speed);
    }
}


var running;
var currentImg = 0;  
var theImages = new Array("./imgDes/1.jpg","./imgDes/2.jpg","./imgDes/4.jpg","./imgDes/11.jpg");

function cycleImage() {
   var figImg = document.getElementById("pic");
   if(document.images) {
	currentImg++;	
	currentImg = currentImg%4;
	figImg.src = theImages[currentImg];
   }
}
 	
function init() {
	running = setInterval("cycleImage()", 2000);  
}
window.onload = init;
