<h3 id="title">Katherine Gorge & Edith Falls Full-Day Tour from Darwin
</h3>
<p>Discover a network of 13 gorges carved into billion-year-old sandstone rock by the Katherine River of Nitmiluk National Park on this scenic full-day sightseeing tour from Darwin.

During the tour you will learn about the Aboriginal culture abundant in the area and see rock art sites dotted throughout the park. With its rugged landscapes, dramatic waterfalls, and lush gorges, the Nitmiluk National Park is a haven for nature lovers, with plentiful flora and fauna.

Travel south through tropical country, passing former World War II airstrips en route to the Adelaide River. Make a short stop at the Adelaide River War Cemetery to pay your respects to those killed in the air raids of Darwin in the 1940s.

Continue to Edith Falls for a refreshing swim in the paperbark and pandana-lined waterhole (conditions permitting). After an inclusive lunch, enjoy a 2-hour scenic cruise through the Katherine Gorge, looking for freshwater crocodiles sunning themselves on the banks of the river. Keep your eyes open for the many interesting species of birdlife.

Enjoy a brief tour of Katherine, hub of the region’s cattle and farming industries. Then, travel via the historic mining town of Pine Creek to an evening stop along the Adelaide River. From May through September lunch can be taken on board your cruise, while dinner is available at your own expense at the evening stop, before your transfer back to Darwin.</p>